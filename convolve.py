﻿# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 22:07:17 2019

@author: Nice Micro
"""
import numpy as np
#%%
class conv_layer:
    """
    A simple implementation of a convolutional layer in 2D
    """
    def __set_kernel(self, new_kernel):
        """
        Called whenever kernel is modified, puts the kernel the four dimensional
        array with dimensions filter number, input depth, kernel size, kernel size
        and sets up the corresponding size variables.
        """
        self.K = new_kernel
        self.f, self.d, self.ks, self.ks = self.K.shape
        #filter number, input depth, kernel size
    
    def __init__(self, out_layer = 3, inp_layer = 3, kernel_size = 3, pad = 0, stride = 1):
        """
        initialize the convolution layer with a square kernel (default 3 × 3)
        with a number of input and output layers (all default to 3)
        """        
        self.__set_kernel(np.zeros((out_layer, inp_layer, kernel_size,kernel_size), dtype = 'float32'))
        self.bias = np.zeros(out_layer, dtype ='float32')
        #we have no images for this layer loaded yet
        self.m = 0
        self.X = False
        self.xs = 0 #we have no input dimensions for this layer yet     
        self.pad = max(0, min(pad, kernel_size-1))
        self.stride = max(1, stride)
        self.Y = False
        self.dY = False
        self.ys = 0
        
    def get_kernel(self):
        return self.K
    
    def get_bias(self):
        return self.bias
    
    def get_reshaped_inp(self):
        X_resh, indexes = self.__im2col(self.pad, self.stride)
        return X_resh
    
    def get_inp(self):
        return self.X
    
    def get_reshaped_kern(self):
        return self.K[...,::-1,::-1].reshape(self.f,-1)
    
    def get_output(self):
        return self.Y
    
    def get_batch_size(self):
        return self.m
    
    def save_to_text(self, filename):
        """
        Saves three files: one for the convolution map, one for the bias term in
        the text files as filename + "_K.txt" and filename + "_b.txt".
        Dimensions of the Kernel is saved to filename + "_s.txt"
        """
        np.savetxt(filename + '_s.txt', self.K.shape, fmt='%d')
        np.savetxt(filename + '_K.txt', self.K.reshape((-1, self.ks)), fmt='%10.5f')
        np.savetxt(filename + '_b.txt', self.bias, fmt='%10.5f')
        
    def load_from_text(self, filename):
        """
        Loads three files: one for the convolution map, one for the bias term in
        the text files as filename + "_K.txt" and filename + "_b.txt".
        Dimensions of the Kernel are from filename + "_s.txt"
        Returns:
            True if succeeds, False, if fails because of non-square dimensions
        """
        d, f, x, y = tuple(np.loadtxt(filename + '_s.txt', dtype = int))
        if x != y:
            return False
        K_flat = np.loadtxt(filename + '_K.txt', dtype = "float32")
        self.__set_kernel(K_flat.reshape((d, f, x, y)))
        self.bias = np.loadtxt(filename + '_b.txt', dtype = "float32")
        return True
    
    def set_kernel_rnd(self, variance = 0.05):
        """
        sets the kernel to a random matrix with a Gaussian distribution with the
        given variance (default to 0.05)
        """
        self.__set_kernel(np.random.normal(0,variance,(self.K.shape)).astype('float32'))
    
    def set_kernel(self, W, b):
        """
        Sets the kernel to matrix W and bias to vector b. W should be dimensions
        f, d, ks, ks where f = filter num, d = input depth, ks = kernel size,
        b should have the dimension f.
        Returns:
            False if dimensions do not match, True if successful.
        """
        f, d, x, y = W.shape
        b_f,  = b.shape
        if x != y or f != b_f or x != self.ks or f != self.f or d != self.d:
            return False
        self.__set_kernel(W)
        self.bias = b
        return True
        
    def set_inp(self, X):
        """
        Gets an image (input data) batch to convolve.
        Returns:
            True if succeeds, False, if fails because of non-square dimensions
        """
        m, depth, img_x, img_y = X.shape
        if depth != self.d or img_x != img_y:
            return False
        self.m = m #number of inputs in this batch
        self.xs = img_x #dimensions of input data by axis
        self.X = X
        return True
    
    def __im2col_loops(self, Mat, pad, stride):
        """
        Reshapes the input into a 2D matrix for convolution, based on the naive
        implementation with for loops.
        """
        X_pad = np.pad(Mat, ((0,0),(0,0),(pad,pad),(pad,pad)), mode= 'constant')        
        m, d, x, y = X_pad.shape
        resh_x = self.ks **2 * d #same as the y dimension of the flat K
        resh_imdim = (x - self.ks)//stride +1
        # how many times do we do a convolution operation on the image by one axis
        resh_y = (resh_imdim)**2 
        X_interm = np.zeros((m, resh_x, resh_y), dtype = Mat.dtype)
        for i in range(resh_imdim):
            for j in range(resh_imdim):
                x_pos = i*stride
                y_pos = j*stride
                patch = X_pad[:, :, x_pos:x_pos+self.ks, y_pos:y_pos+self.ks]
                patch = patch.reshape((m, resh_x))
                X_interm[:, :, i*resh_imdim + j] = patch[:,:]
        return np.transpose(X_interm, (1,0,2)).reshape((resh_x, resh_y*m))
    
    def __im2col_indexes(self, m, d, size, stride):
        """
        Gets the indexes to reshape the input into a 2D matrix for convolution.
        based on the sliding-broadcasting method from Divakar on
        https://stackoverflow.com/questions/30109068/implement-matlabs-im2col-sliding-in-python
        Returns: the matrix of indexes
        """
        #Indexes for the slices in one layer that is shaped like the kernel (ks × ks)
        X_a = np.arange(self.ks)[:,None]*size + np.arange(self.ks)
        #Slices in all layers (d × ks * ks)
        X_b = np.arange(d)[:,None]*size*size + X_a.ravel()
        #Find all possible slice starting points in one example
        X_c = np.arange(0,size-self.ks+1,stride)[:,None]*size + np.arange(0,size-self.ks+1,stride)
        #Find all possible slice starting points in all examples
        X_d = np.arange(m)[:,None]*size*size*d + X_c.ravel()
        #Combine the slices with all starting points
        X_e = X_b.ravel()[:,None]+X_d.ravel()
        return  X_e
    
    def __im2col(self, Mat, pad, stride):
        """
        Reshapes the Mat matrix into a 2D matrix for convolution, based on the 
        sliding-broadcasting method from Divakar on
        https://stackoverflow.com/questions/30109068/implement-matlabs-im2col-sliding-in-python
        """
        X_pad = np.pad(Mat, ((0,0),(0,0),(pad,pad),(pad,pad)), mode= 'constant')
        m, d, size, size = X_pad.shape
        X_e = self.__im2col_indexes(m, d, size, stride)
        return np.take(X_pad, X_e)
    
    def convolve(self):
        """
        Executes the convolution on the preloaded data and the set kernel, 
        Padding pads on x and y axes, stride is the stride of the convolution.
        Returns:
            the final result as a four dimensional array, where the dimensions are
            number of item, layer, height and width.
        """
        if self.xs == 0:
            return False
        X_resh = self.__im2col(self.X, self.pad, self.stride)
        K_flat = self.K[...,::-1,::-1].reshape(self.f,-1)
        Y_flat = (K_flat @ X_resh) + self.bias.reshape((self.f,1))
        self.ys = (self.xs + self.pad*2 - self.ks)//self.stride +1
        Y_reshaped = Y_flat.reshape(self.f, self.m, self.ys, self.ys)
        self.Y = np.transpose(Y_reshaped, (1,0,2,3)) #the index order must be changed
        self.dY = np.zeros(self.Y.shape)
        return self.Y
    
    def set_dY(self, dY):
        """
        Receives the derivative of the costfunction by each point of the output layer.
        The shape of dY should be the same as Y: a four dimensional array with the
        dimension of items, layers (= filter number of current conv), height and width
        Returns:
            False if something doesn't match, True if the dY value is received.
        """
        m, d, x, y = np.shape(dY)
        if m != self.m or d != self.f or x != self.ys or y != self.ys or self.xs == 0 or self.ks == 0:
            return False
        self.dY = dY
        return True
    
    def __dX_naive(self):
        """
        A naive implementation of calculating the derivatives of the cost function
        by the input matrix elements.
        """
        dX = np.zeros(self.X.shape).ravel() # we will get the numbers in a flat shape first
        
        K_flat = self.K[...,::-1,::-1].reshape(self.f,-1)
        dY_flat = (np.transpose(self.dY, (1,0,2,3))).reshape(self.f, -1)
        dX_addends = (K_flat.T @ dY_flat)
        #the reverse operation now used to get the dX, but the reshaped one.
        #get the indexes to sum up the X based on the im2col methods
        m, d, size, size = self.X.shape
        X_indexes = self.__im2col_indexes(m, d+self.pad*2, size, self.stride)
        #The next part is based on the following thread on stackoverflow:
        #https://stackoverflow.com/questions/48668885/speeding-up-fancy-indexing-with-numpy
        # flatten your arrays
        dX_a_values = dX_addends.ravel()
        dX_a_ind = X_indexes.ravel()
        s = np.argsort(dX_a_ind)  # these are the indices that will sort your Index array
        
        v_sorted = dX_a_values[s].copy()  # the copy here orders the values in memory instead of just providing a view
        i_sorted = dX_a_ind[s].copy()
        searches = np.searchsorted(i_sorted, np.arange(0, i_sorted[-1] + 2))
        # 1 greater than your max, this gives you your array end...
        for i in range(len(searches) -1):
            st = searches[i]
            nd = searches[i+1]
            dX[i] = v_sorted[st:nd].sum()
        return dX.reshape(self.X.shape)
    
    def __dX_tr_conv(self):
        """
        An implementation of calculating the derivaties of the cost function based
        on the inverse operation of a strided and customly padded convolution,
        a transposed convolution operation.
        """
        pad_dY = self.ks - self.pad - 1
        #padding dY is the "opposite" of the padding for X        
        Dil = np.zeros((self.ys*self.stride-self.stride+1, self.ys), dtype=int) 
        Dil[np.arange(0,self.ys*self.stride-self.stride+1, self.stride), np.arange(self.ys)] = 1
        #the matrix for diluting the dY has a 1 in every column, that are spaced out
        #by (stride-1) zeros between them. So the matrix height is ys + (ys-1)*(stride-1)
        dY_dil = Dil @ self.dY @ Dil.T        
        #The diluted dY is what will be convoluted with the 180% rotated
        #kernel, and also the kernel must be transposed for depth and layer. 
        #padding of the diluted dY will happen in the __im2col method
        dY_resh = self.__im2col(dY_dil, pad_dY, 1)
        K_flat = self.K.transpose(1,0,2,3).reshape(self.d,-1)
        #We need to change K into a transposed matrix so its dimensions correspond to
        #the depth of dY (which is the filternumber of the original kernel), and the filter
        #number becomes equal to the depth of X, so the f, d, ks, ks becomes d, f, ks, ks
        dX_flat = (K_flat @ dY_resh)
        dX_reshaped = dX_flat.reshape(self.d, self.m, self.xs, self.xs)
        dX = dX_reshaped.transpose(1,0,2,3) #turn the matrix shape back to m, d, xs, xs
        return dX
    
    def backprop(self, reg_param = 0):
        """
        Calculates the derivative of the cost function by the kernel and the input
        elements based on the derivative received as dY. A regularization parameter
        for K2 regularization can be provided if needed.
        Returns:
            dK: the derivative of the cost function w.r.t. the elements in the kernel
            dbias: the derivatie of the cost function w.r.t. the bias elements
            dX: the derivative of the cost function w.r.t. the input elements
        """
        if self.m == 0 or self.ys == 0:
            return False, False, False
        dX = np.zeros(self.X.shape)
        dK = np.zeros(self.K.shape)
        dbias = np.zeros(self.f)
        
        dbias = np.sum(self.dY, axis=(0,2,3))
        #The derivative of the cost function by the bias is just the sum of all 
        #derivatives of the layer the bias was applied to
        X_resh = self.__im2col(self.X, self.pad, self.stride)
        dY_flat = (np.transpose(self.dY, (1,0,2,3))).reshape(self.f, -1)
        dK_flat = dY_flat @ X_resh.T
        dK = dK_flat.reshape(self.f, self.d, self.ks, self.ks)[...,::-1,::-1]
        dK += (reg_param*self.K)
        #We basically reverse the operation we used to get Y. This way we get the
        #derivatives of the cost function by each element in the kernel,
        #and then regularizes the cost (L2 regularization)
        
        dX = self.__dX_tr_conv()
        return dK, dbias, dX
     
    def update_weights(self, dW, dbias, alpha=1):
        """
        Updates the weights and the bias of the convolution layer by the value
        given in paramaters, where alpha is the learning factor.
        """
        f, d, x, y = dW.shape
        b_f,  = dbias.shape
        if x != y or f != b_f or x != self.ks or f != self.f or d != self.d:
            return False
        self.__set_kernel(self.K - dW*alpha)
        self.bias -= dbias*alpha
        return True
#%%
class ReLU_layer:
    """
    Simple implementation of a tectified linear unit layer.
    """
    def __init__(self):
        self.X = False
        self.dY = False
    
    def set_inp(self, X):
        """
        An input with any dimensions should be fine: it can be an activation for 
        convolved layer or fully connected layer.
        """
        self.X = X
        return True
        
    def get_outp(self):
        """
        Returns the rectified values: 0 if X<=0, X if X>0
        """
        return self.X*(self.X >= 0)
    
    def set_dY(self, dY):
        """
        Sets the derivative of the cost function by the output variables.
        The shape of the derivative should be the same as the input.
        """
        if self.X.shape != dY.shape:
            return False
        self.dY = dY
        return True

    def backprop(self):
        """
        Backpropagates the error, resulting in the derivative of the cost function
        by the inputs.
        """
        dX = self.dY * (self.X >= 0)
        return dX
    
#%%
class max_pool_layer:
    """
    Simple implementation of a max pooling layer.
    """
    def __init__(self, step = 2):
        """
        Step defines the size of a square that will be the basis of the max pooling
        """
        self.step = step
        self.X = False
        self.xs = 0
        self.m = 0
        self.d = 0
        self.dY = False
        
    def set_inp(self, X):
        """
        Set the input X matrix. Must be a square matrix.
        """
        m, d, x, y = X.shape
        if x != y:
            return False
        self.X = X
        self.Xmark = np.zeros(X.shape)
        self.xs = x
        self.m = m
        self.d = d
        return True
    
    def __im2col_indexes(self, m, d, size):
        """
        Gets the indexes to reshape the input into a 2D matrix for convolution.
        based on the sliding-broadcasting method from Divakar on
        https://stackoverflow.com/questions/30109068/implement-matlabs-im2col-sliding-in-python
        Returns: the matrix of indexes
        """
        #Indexes for the slices in one layer that is shaped like the kernel (ks × ks)
        X_a = np.arange(self.step)[:,None]*size + np.arange(self.step)
        #All starting ponts in a layer
        X_b = np.arange(0, size-self.step+1, self.step)[:,None]*size + np.arange(0, size-self.step+1, self.step)
        #Find all possible slice starting points in all layers and all examples
        X_st = np.arange(m)[:,None,None]*size*size*d + np.arange(d)[None,:,None]*size*size + X_b.ravel()[None,None,:]
        #Combine the slices with all starting points
        X_e = X_st[...,None]+X_a.ravel()[None,None,None,:]
        return  X_e
    
    def __im2col(self, Mat):
        """
        Transforms a tensor into the tensor of the flattened patches on where the max
        pooling will take place.
        """
        m, d, x, y = Mat.shape
        Indexes = self.__im2col_indexes(m, d, x)
        return np.take(Mat, Indexes)
    
    def __col2im(self, Mat):
        """
        From the result of the max pooling, it makes the tensor containing
        the small square matrices.
        """
        m, d, s = Mat.shape
        if s == ((self.xs - self.step) // self.step +1)**2:
            return Mat.reshape(m, d, ((self.xs - self.step) // self.step +1), ((self.xs - self.step) // self.step +1))
        else:
            return False
        
    def __im2colrev(self, Mat):
        """
        It does the opposite what the __im2col function does: gets the patches
        and restores the original matrix from that
        """
        m, d, x, y = Mat.shape
        if x == ((self.xs-self.step) // self.step +1)**2 and y == self.step**2:
            indexes = self.__im2col_indexes(m, d, self.xs)
            Ot = np.zeros(self.X.ravel().shape, dtype = int)
            Ot[indexes.ravel()] = Mat.ravel()
            Ot = Ot.reshape(self.X.shape)
            return Ot
        else:
            return False

    def get_outp(self):
        """
        Performs the max pooling on the input tensor by the size that's been defined,
        sets up the matrix that contains which original X elements are moved to
        the max pooled output, and returns:
            the max pooled tensor with dimensions (samples, layers, s_new, s_new)
            where the size s_new is (Xs-step)//step+1
        """
        X_col = self.__im2col(self.X)
        X_max = np.max(X_col, axis = 3)
        X_argmax = np.argmax(X_col, axis = 3).astype(int)
        #we got the position of the maximums in the flattened version
        Xwhere = (X_argmax.ravel()[:,None]*[0,1])
        Xwhere[:,0] = np.arange(X_argmax.ravel().size)
        #we made a 2D array to contain the max positions and the row numbers
        Xmark_flat = np.zeros(X_col.shape, dtype = int).reshape(-1,self.step**2)
        Xmark_flat[Xwhere[:,0],Xwhere[:,1]] = 1
        Xmark_flat = Xmark_flat.reshape(X_col.shape)
        Y = self.__col2im(X_max)
        Xmark = self.__im2colrev(Xmark_flat)
        self.Xmark = Xmark
        return Y
    
    def set_dY(self, dY):
        """
        Sets the derivative of the cost function by the outputs.
        The shape should be the same as for the outputs.
        """
        m, d, x, y = dY.shape
        if m == self.m and d==self.d and x==y and x==(self.xs-self.step) // self.step +1:
            self.dY = dY
            return True
        else:
            return False
        
    def __upsize_indexes(self, m, d, s, step):
        """
        This private function calcuates that which elements in the input matrix
        could contribute to the specific output element.
        """
        small = (s - step) // step +1
        #Getting the big matrix filled with small matrix references
        dX_a = (np.arange(s)//step)[:,None]*small + (np.arange(s)//step)
        #Starting point for each sample and each layer
        dX_b = np.arange(m)[:,None] * d*small**2 + np.arange(d) * small**2
        return dX_b[:,:,None,None]+ dX_a[None,None,:,:]
    
    def backprop(self):
        """
        Propagates the error. Calculates the derivative of the cost function by
        the the inputs. If an input did not make it to the pooled layer, its
        derivative is zero.
        """
        Indexes = self.__upsize_indexes(self.m,self.d,self.xs,self.step)
        dX = self.dY.ravel()[Indexes]
        return dX * self.Xmark
#%%
class fc_layer:
    """
    Simple implementation of a fully connected layer
    """
    def __init__(self, inp, out):
        """
        inp is the dimension of the input layers, out is for the output layers.
        """
        self.inp = inp
        self.out = out
        self.W = np.zeros((inp, out), dtype="float32")
        self.bias = np.zeros((out), dtype = "float32")
        self.Y = False
        self.m = 0
    
    def set_weights(self, W, bias):
        """
        sets the W as the weight matrix and bias as the bias vector. The shape
        should reflect the input and output dimensions.
        Returns:
            True, if the shapes are good and weights and the bias are uptaded.
            False, if the shapes don't match up, and the
        """
        if self.W.shape == W.shape and self.bias.shape == bias.shape:
            self.W = W
            self.bias = bias
            return True
        else:
            return False
    
    def get_weights(self):
        return self.W
    
    def get_bias(self):
        return self.bias
      
    def get_inp(self):
        return self.X
        
    def get_output(self):
        return self.Y
    
    def get_batch_size(self):
        return self.m
    
    def save_to_text(self, filename):
        """
        Saves three files: one for the weight matrix, one for the bias term in
        the text files as filename + "_W.txt" and filename + "_b.txt".
        """
        np.savetxt(filename + '_W.txt', self.W, fmt='%10.5f')
        np.savetxt(filename + '_b.txt', self.bias, fmt='%10.5f')
        
    def load_from_text(self, filename):
        """
        Loads three files: one for the weight matrix, one for the bias term in
        the text files as filename + "_W.txt" and filename + "_b.txt".
        Returns:
            True if succeeds, False, if fails because of non-square dimensions
        """
        W = np.loadtxt(filename + '_W.txt', dtype = "float32")
        bias = np.loadtxt(filename + '_b.txt', dtype = "float32")
        if self.W.shape == W.shape and self.bias.shape == bias.shape:
            self.W = W
            self.bias = bias
            return True
        else:
            return False
    
    def set_weights_rnd(self, variance = 0.05):
        """
        sets the weight matrix and the biases to a random matrix with a Gaussian
        distribution with the given variance (default to 0.05)
        """
        self.W = np.random.normal(0,variance,(self.W.shape)).astype('float32')
        self.bias = np.random.normal(0,variance,(self.bias.shape)).astype('float32')